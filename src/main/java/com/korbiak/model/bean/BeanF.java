package com.korbiak.model.bean;

public class BeanF implements BeanValidator{
    private String name;
    private String value;

    @Override
    public void validate() {
        name = "null";
        if (value.length() < 1) {
            value = "null";
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
