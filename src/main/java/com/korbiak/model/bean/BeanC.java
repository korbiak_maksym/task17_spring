package com.korbiak.model.bean;

import org.springframework.beans.factory.annotation.Value;


public class BeanC implements BeanValidator {
    @Value("${beanC.name}")
    private String name;
    @Value("${beanC.value}")
    private String value;

    private void init(){
        System.out.println("init BeanC");
    }

    @Override
    public void validate() {
        name = "null";
        if (value.length() < 1) {
            value = "null";
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    private void destroy(){
        System.out.println("destroy BeanC");
    }
}
