package com.korbiak.model.part2.otherBeans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class OtherBeanA {

    @Override
    public String toString() {
        return "OtherBeanA{" + this.hashCode() + "}";
    }
}
