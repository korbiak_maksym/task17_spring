package com.korbiak.model.part2.implBeans;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class ImplBeanB implements BeanInt {

    @Override
    public String getName() {
        return "ImplBeanB";
    }
}
