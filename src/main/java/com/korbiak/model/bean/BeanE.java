package com.korbiak.model.bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private String name;
    private String value;

    @Override
    public void validate() {
        name = "null";
        if (value.length() < 1) {
            value = "null";
        }
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("@PostConstruct ");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("@PreDestroy");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
