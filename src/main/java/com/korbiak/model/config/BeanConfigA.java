package com.korbiak.model.config;


import com.korbiak.model.bean.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@Import(BeanConfigB.class)
@PropertySource("classpath:my.properties")
public class BeanConfigA {

    @Bean(name = "FirstA")
    @DependsOn(value = {"D", "B", "C"})
    public BeanA getBeanAFirst(BeanB beanB, BeanC beanC) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanC.getValue());
        return beanA;
    }

    @Bean(name = "SecondA")
    @DependsOn(value = {"D", "B", "C"})
    public BeanA getBeanASecond(BeanB beanB, BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }


    @Bean(name = "ThirdA")
    @DependsOn(value = {"D", "B", "C"})
    public BeanA getBeanAThird(BeanC beanC, BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanC.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }

    @Bean(name = "FirstE")
    public BeanE getBeanEFirst(@Qualifier("FirstA") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean(name = "SecondE")
    public BeanE getBeanESecond(@Qualifier("SecondA") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean(name = "ThirdE")
    public BeanE getBeanEThird(@Qualifier("ThirdA") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }
}
