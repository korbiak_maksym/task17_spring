package com.korbiak.model.config;

import com.korbiak.model.bean.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;


@Configuration
public class BeanConfigB {
    @Bean(name = "B", initMethod = "init", destroyMethod = "destroy")
    public BeanB getBeanB() { return new BeanB(); }

    @Bean(name = "D", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD();
    }

    @Bean(name = "C", initMethod = "init", destroyMethod = "destroy")
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(name = "F")
    @Lazy
    public BeanF getBeanF() {
        return new BeanF();
    }

    //To resolve ${} in @Value
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public static ListOfBeans beanPost(){
        return new ListOfBeans();
    }

    @Bean
    public MyBeanPostProcessor getMyBean(){
        return new MyBeanPostProcessor();
    }
}
