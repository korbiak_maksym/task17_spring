package com.korbiak.model.part2.beans1;

import com.korbiak.model.part2.otherBeans.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanB {

    OtherBeanB otherBeanB;

    @Autowired
    public BeanB(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "otherBeanB=" + otherBeanB +
                '}';
    }
}
