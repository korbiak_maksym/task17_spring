package com.korbiak.model.part2.implBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MaxBean {
    @Autowired
    List<BeanInt> list;

    public List<BeanInt> getList() {
        return list;
    }
}
