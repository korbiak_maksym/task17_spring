package com.korbiak.controller;

import com.korbiak.model.Model;


public class Controller {

    private Model model;

    public Controller() {
        this.model = new Model();
    }

    public String start() {
        return model.start();
    }


    public String start2() {
        return model.start2();
    }
}
