package com.korbiak.model.part2.otherBeans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanC {
    @Override
    public String toString() {
        return "OtherBeanC{" + this.hashCode() + "}";
    }
}
