package com.korbiak.model.part2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ConfigC {

    @Bean
    @Profile("dev")
    public com.korbiak.model.part2.Bean getDataSourceForDevelopment() {
        String url = "jdbc:mysql://localhost:3306/test_db";
        String user = "root";
        String password = "root";
        return new com.korbiak.model.part2.Bean(url, user, password);
    }

    @Bean
    @Profile("prod")
    public com.korbiak.model.part2.Bean getDataSourceForProduction() {
        String url = "jdbc:mysql://localhost:4105/lviv_db";
        String user = "recruiter";
        String password = "X23_q1<55";
        return new com.korbiak.model.part2.Bean(url, user, password);
    }
}
