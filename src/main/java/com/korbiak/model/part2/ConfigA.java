package com.korbiak.model.part2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.korbiak.model.part2.otherBeans"})
public class ConfigA {
}
