package com.korbiak;

import com.korbiak.model.part2.ConfigA;
import com.korbiak.view.View;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    public static void main(String[] args) {
        new View().show();
    }
}
