package com.korbiak.model.bean;

import org.springframework.beans.factory.annotation.Value;


public class BeanD implements BeanValidator {
    @Value("${beanD.name}")
    private String name;
    @Value("${beanD.value}")
    private String value;

    private void init(){
        System.out.println("init BeanD");
    }

    @Override
    public void validate() {
        name = "null";
        if (value.length() < 1) {
            value = "null";
        }
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    private void destroy(){
        System.out.println("destroy BeanD");
    }
}
