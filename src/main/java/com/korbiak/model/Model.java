package com.korbiak.model;


import com.korbiak.model.bean.*;

import com.korbiak.model.part2.Bean;
import com.korbiak.model.part2.ConfigA;
import com.korbiak.model.part2.ConfigC;
import com.korbiak.model.part2.otherBeans.OtherBeanA;
import com.korbiak.model.part2.otherBeans.OtherBeanB;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.korbiak.model.config.BeanConfigA;

public class Model {

    public String start() {
        String answer = "";
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(BeanConfigA.class);
        BeanA beanA = applicationContext.getBean("FirstA", BeanA.class);
        BeanE beanE = applicationContext.getBean("FirstE", BeanE.class);
        answer += beanA.toString() + " = ";
        answer += beanE.toString();
        applicationContext.close();
        return answer;
    }

    public String start2() {
        String answer = "";
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("dev");
        context.register(ConfigC.class);
        context.refresh();
        answer += context.getBean(Bean.class);
        return answer;
    }
}

