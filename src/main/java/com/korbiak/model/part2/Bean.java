package com.korbiak.model.part2;

public class Bean {
    private String url;
    private String user;

    public Bean(String url,
                      String user, String password) {
        this.url = url;
        this.user = user;
    }

    public String toString() {
        return "url=" + url + "user=" + user;
    }
}
