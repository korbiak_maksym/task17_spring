package com.korbiak.model.bean;

public interface BeanValidator {
    void validate();
}
