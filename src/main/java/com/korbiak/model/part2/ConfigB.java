package com.korbiak.model.part2;

import com.korbiak.model.part2.beans3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = {"com.korbiak.model.part2.beans2",
        "com.korbiak.model.part2.beans3"},
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = BeanE.class)
)
public class ConfigB {

}
