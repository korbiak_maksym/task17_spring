package com.korbiak.model.part2.beans1;

import com.korbiak.model.part2.otherBeans.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanA {

    @Autowired
    OtherBeanA otherBeanA;

    @Override
    public String toString() {
        return "BeanA{" +
                "otherBeanA=" + otherBeanA +
                '}';
    }
}
